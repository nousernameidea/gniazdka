package tb.sockets.server;

import java.net.ServerSocket;

//import tb.sockets.server.Game;
//import tb.sockets.server.Game.Player;



public class Konsola {

    public static void main(String[] args) throws Exception {
        ServerSocket sSock = new ServerSocket(6666);
        System.out.println("Serwer Uruchomiony...");
        try {
            while (true) {
                Game game = new Game();
                Game.Player playerX = game.new Player(sSock.accept(), 'X');
                Game.Player playerO = game.new Player(sSock.accept(), 'O');
                playerX.setOpponent(playerO);
                playerO.setOpponent(playerX);
                game.currentPlayer = playerX;
                playerX.start();
                playerO.start();
            }
        } finally {
            sSock.close();
        }
    }
}
