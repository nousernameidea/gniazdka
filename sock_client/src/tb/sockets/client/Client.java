package tb.sockets.client;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Client {

    private JFrame frame = new JFrame();
    private JLabel messageLabel = new JLabel("");
    private String sign;
    private String opponentsign;

    private MyButton[] field = new MyButton[9];
    private MyButton currentButton;

    private static int PORT = 6666;
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;

  
    public Client(String serverAddress) throws Exception {

        //��czenie
        socket = new Socket(serverAddress, PORT);
        in = new BufferedReader(new InputStreamReader(
            socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);

        //okno
        messageLabel.setBackground(Color.lightGray);
        frame.getContentPane().add(messageLabel, "South");

        OrderPane fieldPanel = new OrderPane();
        for (int i = 0; i < field.length; i++) {
            final int j = i;
            field[i] = new MyButton();
            field[i].addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    currentButton = field[j];
                    out.println("MOVE " + j);}
            });
            fieldPanel.add(field[i]);
        }
        frame.getContentPane().add(fieldPanel, "Center");
    }
   
    public void play() throws Exception {
        String response;
        try {
            response = in.readLine();
            if (response.startsWith("WELCOME")) {
                char mark = response.charAt(8);
                sign = new String(mark == 'X' ? "X" : "O");
                opponentsign  = new String(mark == 'X' ? "O" : "X");
                frame.setTitle("Kolko i Krzyzyk - Gracz " + mark);
            }
            while (true) {
                response = in.readLine();
                if (response.startsWith("VALID_MOVE")) {
                    messageLabel.setText("Poprawny ruch, teraz przeciwnik");
                    currentButton.mysetText(sign);
                    currentButton.repaint();
                } else if (response.startsWith("OPPONENT_MOVED")) {
                    int loc = Integer.parseInt(response.substring(15));
                    field[loc].setText(opponentsign);
                    field[loc].repaint();
                    messageLabel.setText("Tw�j ruch");
                } else if (response.startsWith("VICTORY")) {
                    messageLabel.setText("Wygra�e�");
                    break;
                } else if (response.startsWith("DEFEAT")) {
                    messageLabel.setText("Przegra�e�");
                    break;
                } else if (response.startsWith("TIE")) {
                    messageLabel.setText("Remis");
                    break;
                } else if (response.startsWith("MESSAGE")) {
                    messageLabel.setText(response.substring(8));
                }
            }
            out.println("QUIT");
        }
        finally {
            socket.close();
        }
    }

    private boolean wantsToPlayAgain() {
        int response = JOptionPane.showConfirmDialog(frame,
            "Czy chcesz zagra� jeszcze raz?",
            "Rewan�?",
            JOptionPane.YES_NO_OPTION);
        frame.dispose();
        return response == JOptionPane.YES_OPTION;
    }

    
    static class MyButton extends JButton {

        public MyButton() {
        	super();
        }

        public void mysetText(String xoro) {
            this.setText(xoro);
        }
    }   
    
    //main
    public static void main(String[] args) throws Exception {
        while (true) {
            String serverAddress = (args.length == 0) ? "localhost" : args[1];
            Client client = new Client(serverAddress);
            client.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            client.frame.setSize(400, 400);
            client.frame.setVisible(true);
            client.frame.setResizable(false);
            client.play();
            if (!client.wantsToPlayAgain()) {
                break;
            }
        }
    }
}